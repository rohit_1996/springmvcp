package com.MyMavenProject.SpringMVCP;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller1 {
	
	@RequestMapping("/abc")
	public String control() {
		return "hello java";
	}

}
