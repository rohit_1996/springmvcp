package com.MyMavenProject.SpringMVCP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMvcpApplication.class, args);
		
	}

}
